# Config
Az api URL-t az /src/environments/environment.ts alatt találod. \
Alapvetően az 1080-as port mögött figyel, ha a dockerben módosítasz, ne felejtsd el itt sem.

# Debug
Komponens debug console-ból: Kijelölni a böngészőben a komponens belsejét, majd: RegisterComponent = ng.getContext($0) \
Mutatja a private változót is \
Van browser tool is (https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh)

# Validáció 
Komponensben, Validators osztály tartalmazza előre definiáltakat. \
A html-ben kell hozzá a formControlName attribútum.

# Guard
Olyasmi, mint a middleware. be lehet állítani, hogy routolásnál pl csak a bejelentkezett felhasználó láthassa az adott oldalt.

# HTTP Interceptor
Interfész, ami a http requesteknél implementálódik. \ 
Lehet sajátot is írni, ami jól jön, ha pl minden kérésbe bele akarok tenni pl egy tokent. \
Provider kapcsán tudom beletenni, ami fontos: az összes kérésbe bele fogja tenni

# Observable
Lényegében az Observer tervezési minta megvalósítása. \ 
Az Observer értesíteni tudja a rá feliratkozókat (subscribe), ha változott az állapota. \
A Promisehoz képest annyi különbség van, hogy a Promise egyszer meghívódik, utána ő már nem fut le többet. Az Observable folyamatosan figyel azután is, hogy az esemény megtörtént. Az Observable-ket lehet törölni is, hogy ne figyeljenek tovább.



