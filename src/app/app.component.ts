import { Component } from '@angular/core';
import {UserService} from "./user.service";
import {Router} from "@angular/router";
import {User} from "./_models/user";
import {NgbConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-frontend';
  currentUser: User = new User;

  constructor(
      private userService: UserService,
      private router: Router
  ) {
    this.userService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout()
  {
    this.userService.logout();
    this.router.navigate(['/login']);
  }
}
