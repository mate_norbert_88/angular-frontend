import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.formBuilder.group({
    name: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]]
  });

  submitted = false;
  message: any;

  constructor(
      private formBuilder: FormBuilder,
      private userService: UserService
  ) {}

  ngOnInit(): void {
  }

  get formControls() {
    return this.registerForm.controls;
  }

  registerSubmit() : void {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.userService.register(this.registerForm.value).subscribe(
        data => {
          this.message = {text: "Registration successful", cssClass: 'alert alert-success'};
        },
        error => {
          this.message = {text: "Registration failed: " + JSON.stringify(error), cssClass: 'alert alert-danger'}
        }
    )
  }

}
