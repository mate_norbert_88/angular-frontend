import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import { map } from 'rxjs/operators';
import {BehaviorSubject, Observable} from "rxjs";
import {User} from "./_models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(<string>localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  register(user: any) {
    return this.http.post(`${environment.apiUrl}/register`, user);
  }

  login(name: string, password: string) {
    return this.http.post(`${environment.apiUrl}/login`, {name, password}).pipe(map(lumenToken => {
      let user = {name: name, token: lumenToken};
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.currentUserSubject.next(<User>user);
      return user;
    }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    // @ts-ignore
    this.currentUserSubject.next(null);
  }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }
}
