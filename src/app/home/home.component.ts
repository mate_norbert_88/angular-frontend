import { Component, OnInit } from '@angular/core';
import {User} from "../_models/user";
import {UserService} from "../user.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: User;
  users: Array<User> = [];
  errorMessage: string = '';

  constructor(
      private userService: UserService
  ) {
    this.user = this.userService.currentUserValue;
  }

  ngOnInit(): void {
    this.loadAllUsers();
  }

  private loadAllUsers()
  {
    this.userService.getAll().subscribe(users => {
      return this.users = users;
    }, error => this.errorMessage = JSON.stringify(error));
  }

}
