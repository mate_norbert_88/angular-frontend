import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    name: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]]
  });
  submitted = false;
  message: any;
  returnUrl: string = '';
  private test: string = '';

  constructor(
      private formBuilder: FormBuilder,
      private userService: UserService,
      private router: Router,
      private route: ActivatedRoute
  ) {
    if (this.userService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get formControls() {
    return this.loginForm.controls;
  }

  submitLoginForm() : void
  {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.userService.login(this.formControls.name.value, this.formControls.password.value).pipe(first()).subscribe(
        data => {
          this.router.navigate([this.returnUrl])
        },
        error => {
          this.message = {text: "Login failed: " + JSON.stringify(error), cssClass: 'alert alert-danger'}
        }
    )
  }

}
